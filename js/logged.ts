///  <reference path="jquery.d.ts"/>

class Logged {
	static readonly API_URL_ORDER = 'https://api.dotykacka.cz/api/order/';
    static readonly API_URL_EMPLOYEE = 'https://api.dotykacka.cz/api/employee/';
    static readonly CORS = 'https://cryptic-headland-94862.herokuapp.com/' /*'https://cors-anywhere.herokuapp.com/'*/;
    //static readonly PAYMENT_UNPAID = 0;
    static readonly PAYMENT_CASH = '900000001';
    static readonly PAYMENT_CARD = '900000002';
    static readonly PAYMENT_FOODSTAMP = '900000004';

	private cloudId: string;
	private accessToken: string;
	private employees: any[] = [];
	private todayDiv: JQuery;
    private yesterdayDiv: JQuery;
    private employeeNameInput: JQuery;
	
	public constructor () {
		this.cloudId = window.sessionStorage.getItem('cloudId');
		this.accessToken = window.sessionStorage.getItem('accessToken');
		this.todayDiv = $('#today');
		this.yesterdayDiv = $('#yesterday');
		this.employeeNameInput = $('#employeeName');
		
		if(!this.cloudId || !this.accessToken) {
			window.location.href = 'index.html';
		}
		
		this.registerEvents();
	}

	private hidePreloader () {
        $('#preloader').fadeOut('slow',function(){$(this).remove();});
	}

    private getYesterdaysDate() {
        let date = new Date();
        date.setDate(date.getDate()-1);
        return date.getFullYear() + '/'+ date.getMonth() +'/'+ date.getDate();
    }

    private getCurrentDate() {
        let date = new Date();
        return date.getFullYear() + '/'+ date.getMonth() +'/'+ date.getDate();
    }

    private done() {
		let _this = this;
        for (let key in this.employees) {
            let value = this.employees[key];

            let employeeTodayDiv = $('<div class="employee"></div>');
            let employeeYesterdayDiv = $('<div class="employee"></div>');
            employeeTodayDiv.append(value.name + '<br>');
            employeeTodayDiv.append('<strong>Hotovost: </strong>' + value.today.cash + ' Kč<br>');
            employeeTodayDiv.append('<strong>Kartou: </strong>' + value.today.card + ' Kč<br>');
            employeeTodayDiv.append('<strong>Stravenkami: </strong>' + value.today.foodstamp + ' Kč<br>');
            employeeTodayDiv.append('<strong>Celkem: </strong>' + (value.today.foodstamp + value.today.card + value.today.cash) + ' Kč<br>');

            employeeYesterdayDiv.append(value.name + '<br>');
            employeeYesterdayDiv.append('<strong>Hotovost: </strong>' + value.yesterday.cash + ' Kč<br>');
            employeeYesterdayDiv.append('<strong>Kartou: </strong>' + value.yesterday.card + ' Kč<br>');
            employeeYesterdayDiv.append('<strong>Stravenkami: </strong>' + value.yesterday.foodstamp + ' Kč<br>');
            employeeYesterdayDiv.append('<strong>Celkem: </strong>' + (value.yesterday.foodstamp + value.yesterday.card + value.yesterday.cash) + ' Kč<br>');

            employeeTodayDiv.append('<br>');
            employeeYesterdayDiv.append('<br>');

            _this.todayDiv.append(employeeTodayDiv);
            _this.yesterdayDiv.append(employeeYesterdayDiv);
        }
	}
	
	private registerEvents() {
		let _this = this;
        $.ajaxSetup({
            'headers': {
                'Authorization': "Bearer " + this.accessToken,
				'Accept': 'application/json'
            }
        });

        $.get(Logged.CORS + Logged.API_URL_EMPLOYEE + this.cloudId, {'enabled': '1'}, function (resp: any[]) {
            resp.forEach(function (val) {
            	_this.employees[val.userid] = {
                    'name': val.canonicalname,
					'today': {
                        'cash': 0,
                        'card': 0,
                        'foodstamp': 0
					},
					'yesterday': {
                        'cash': 0,
                        'card': 0,
                        'foodstamp': 0
					}
                };
            });

           	$.get(Logged.CORS + Logged.API_URL_ORDER + _this.cloudId, {'dateRange': _this.getYesterdaysDate() +'-'+ _this.getYesterdaysDate()}, function (data: any[]) {
                //_this.hidePreloader();
				data.forEach(function (value) {
                    switch(value.order.paymenttypeid) {
                        case Logged.PAYMENT_CASH:
                            _this.employees[value.order.employeeid].yesterday.cash += value.order.totalvaluerounded;
                            break;
                        case Logged.PAYMENT_CARD:
                            _this.employees[value.order.employeeid].yesterday.card += value.order.totalvaluerounded;
                            break;
                        case Logged.PAYMENT_FOODSTAMP:
                            _this.employees[value.order.employeeid].yesterday.foodstamp += value.order.totalvaluerounded;
                            break;
                    }
                });

                $.get(Logged.CORS + Logged.API_URL_ORDER + _this.cloudId, {'dateRange': _this.getCurrentDate() +'-'+ _this.getCurrentDate()}, function (data: any[]) {

                    data.forEach(function (value) {
                        switch(value.order.paymenttypeid) {
                            case Logged.PAYMENT_CASH:
                                _this.employees[value.order.employeeid].today.cash += value.order.totalvaluerounded;
                                break;
                            case Logged.PAYMENT_CARD:
                                _this.employees[value.order.employeeid].today.card += value.order.totalvaluerounded;
                                break;
                            case Logged.PAYMENT_FOODSTAMP:
                                _this.employees[value.order.employeeid].today.foodstamp += value.order.totalvaluerounded;
                                break;
                        }
                    });
                    _this.hidePreloader();
                    _this.done();
                }, 'json');
            }, 'json');
        }, 'json').fail(function () {
            window.location.href = 'index.html';
        });

        this.employeeNameInput.keyup(function () {
			let val = _this.employeeNameInput.val().toString();
            $('.employee').each(function(index, el) {
            	let text = $(el).text();
            	if(val.trim().length === 0) {
                    $(el).show();
                    return;
				}

            	if(text.indexOf(val) !== -1) {
            		$(el).show();
				} else {
                    $(el).hide();
				}
			});
        });
	}
}

$(document).ready(function () {
	new Logged();
});