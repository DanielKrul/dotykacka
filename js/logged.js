///  <reference path="jquery.d.ts"/>
var Logged = /** @class */ (function () {
    function Logged() {
        this.employees = [];
        this.cloudId = window.sessionStorage.getItem('cloudId');
        this.accessToken = window.sessionStorage.getItem('accessToken');
        this.todayDiv = $('#today');
        this.yesterdayDiv = $('#yesterday');
        this.employeeNameInput = $('#employeeName');
        if (!this.cloudId || !this.accessToken) {
            window.location.href = 'index.html';
        }
        this.registerEvents();
    }
    Logged.prototype.hidePreloader = function () {
        $('#preloader').fadeOut('slow', function () { $(this).remove(); });
    };
    Logged.prototype.getYesterdaysDate = function () {
        var date = new Date();
        date.setDate(date.getDate() - 1);
        return date.getFullYear() + '/' + date.getMonth() + '/' + date.getDate();
    };
    Logged.prototype.getCurrentDate = function () {
        var date = new Date();
        return date.getFullYear() + '/' + date.getMonth() + '/' + date.getDate();
    };
    Logged.prototype.done = function () {
        var _this = this;
        for (var key in this.employees) {
            var value = this.employees[key];
            var employeeTodayDiv = $('<div class="employee"></div>');
            var employeeYesterdayDiv = $('<div class="employee"></div>');
            employeeTodayDiv.append(value.name + '<br>');
            employeeTodayDiv.append('<strong>Hotovost: </strong>' + value.today.cash + ' Kč<br>');
            employeeTodayDiv.append('<strong>Kartou: </strong>' + value.today.card + ' Kč<br>');
            employeeTodayDiv.append('<strong>Stravenkami: </strong>' + value.today.foodstamp + ' Kč<br>');
            employeeTodayDiv.append('<strong>Celkem: </strong>' + (value.today.foodstamp + value.today.card + value.today.cash) + ' Kč<br>');
            employeeYesterdayDiv.append(value.name + '<br>');
            employeeYesterdayDiv.append('<strong>Hotovost: </strong>' + value.yesterday.cash + ' Kč<br>');
            employeeYesterdayDiv.append('<strong>Kartou: </strong>' + value.yesterday.card + ' Kč<br>');
            employeeYesterdayDiv.append('<strong>Stravenkami: </strong>' + value.yesterday.foodstamp + ' Kč<br>');
            employeeYesterdayDiv.append('<strong>Celkem: </strong>' + (value.yesterday.foodstamp + value.yesterday.card + value.yesterday.cash) + ' Kč<br>');
            employeeTodayDiv.append('<br>');
            employeeYesterdayDiv.append('<br>');
            _this.todayDiv.append(employeeTodayDiv);
            _this.yesterdayDiv.append(employeeYesterdayDiv);
        }
    };
    Logged.prototype.registerEvents = function () {
        var _this = this;
        $.ajaxSetup({
            'headers': {
                'Authorization': "Bearer " + this.accessToken,
                'Accept': 'application/json'
            }
        });
        $.get(Logged.CORS + Logged.API_URL_EMPLOYEE + this.cloudId, { 'enabled': '1' }, function (resp) {
            resp.forEach(function (val) {
                _this.employees[val.userid] = {
                    'name': val.canonicalname,
                    'today': {
                        'cash': 0,
                        'card': 0,
                        'foodstamp': 0
                    },
                    'yesterday': {
                        'cash': 0,
                        'card': 0,
                        'foodstamp': 0
                    }
                };
            });
            $.get(Logged.CORS + Logged.API_URL_ORDER + _this.cloudId, { 'dateRange': _this.getYesterdaysDate() + '-' + _this.getYesterdaysDate() }, function (data) {
                //_this.hidePreloader();
                data.forEach(function (value) {
                    switch (value.order.paymenttypeid) {
                        case Logged.PAYMENT_CASH:
                            _this.employees[value.order.employeeid].yesterday.cash += value.order.totalvaluerounded;
                            break;
                        case Logged.PAYMENT_CARD:
                            _this.employees[value.order.employeeid].yesterday.card += value.order.totalvaluerounded;
                            break;
                        case Logged.PAYMENT_FOODSTAMP:
                            _this.employees[value.order.employeeid].yesterday.foodstamp += value.order.totalvaluerounded;
                            break;
                    }
                });
                $.get(Logged.CORS + Logged.API_URL_ORDER + _this.cloudId, { 'dateRange': _this.getCurrentDate() + '-' + _this.getCurrentDate() }, function (data) {
                    data.forEach(function (value) {
                        switch (value.order.paymenttypeid) {
                            case Logged.PAYMENT_CASH:
                                _this.employees[value.order.employeeid].today.cash += value.order.totalvaluerounded;
                                break;
                            case Logged.PAYMENT_CARD:
                                _this.employees[value.order.employeeid].today.card += value.order.totalvaluerounded;
                                break;
                            case Logged.PAYMENT_FOODSTAMP:
                                _this.employees[value.order.employeeid].today.foodstamp += value.order.totalvaluerounded;
                                break;
                        }
                    });
                    _this.hidePreloader();
                    _this.done();
                }, 'json');
            }, 'json');
        }, 'json').fail(function () {
            window.location.href = 'index.html';
        });
        this.employeeNameInput.keyup(function () {
            var val = _this.employeeNameInput.val().toString();
            $('.employee').each(function (index, el) {
                var text = $(el).text();
                if (val.trim().length === 0) {
                    $(el).show();
                    return;
                }
                if (text.indexOf(val) !== -1) {
                    $(el).show();
                }
                else {
                    $(el).hide();
                }
            });
        });
    };
    Logged.API_URL_ORDER = 'https://api.dotykacka.cz/api/order/';
    Logged.API_URL_EMPLOYEE = 'https://api.dotykacka.cz/api/employee/';
    Logged.CORS = 'https://cryptic-headland-94862.herokuapp.com/' /*'https://cors-anywhere.herokuapp.com/'*/;
    //static readonly PAYMENT_UNPAID = 0;
    Logged.PAYMENT_CASH = '900000001';
    Logged.PAYMENT_CARD = '900000002';
    Logged.PAYMENT_FOODSTAMP = '900000004';
    return Logged;
}());
$(document).ready(function () {
    new Logged();
});
