///  <reference path="jquery.d.ts"/>

class ApiLogin {
	static readonly API_URL_APITOKEN = 'https://api.dotykacka.cz/oauth/get-apitoken';
	static readonly API_URL_APITOKEN_LOGIN = 'https://api.dotykacka.cz/oauth/login/apitoken';
	static readonly CORS = 'https://cryptic-headland-94862.herokuapp.com/' /*'https://cors-anywhere.herokuapp.com/'*/;
	private emailInput: JQuery;
	private passwordInput: JQuery;
	private cloudIdInput: JQuery;
	private form: JQuery;
	
	public constructor() {
		this.emailInput = $('#email');
		this.passwordInput = $('#password');
		this.cloudIdInput = $('#cloudId');
		this.form = $('#form');
        $('#preloader').hide();
		
		this.registerEvents();
	}
	
	private registerEvents() {
		let _this = this;
		
		this.form.submit(function (e) {
			e.preventDefault();

            let data = {
				'username': _this.emailInput.val(),
				'password': _this.passwordInput.val(),
				'appid': 'touchpo'
			};
            $('#preloader').show();
			
			$.post(ApiLogin.CORS + ApiLogin.API_URL_APITOKEN, data, function (resp) {
                $.ajaxSetup({
                    'headers': {
                        'Authorization': "Basic dG91Y2hwbzp0b3VjaFBvITIwMTZhcGk="
                    }
                });
				$.post(ApiLogin.CORS + ApiLogin.API_URL_APITOKEN_LOGIN, {'grant_type': 'password', 'username': resp.apiToken, 'password': null}, function (response) {
					let accessToken = response.access_token;
					let cloudId: string = _this.cloudIdInput.val().toString();
					
					window.sessionStorage.setItem('accessToken', accessToken);
					window.sessionStorage.setItem('cloudId', cloudId);
					window.location.href = 'logged.html';
				}).fail(function (response2) {
					console.log(response2);
					window.sessionStorage.removeItem('accessToken');
					window.sessionStorage.removeItem('cloudId');
				});
			}).fail(function (resp) {
				console.log(resp);
				alert('Někde se stala chyba, přihlašte se znovu');
                $('#preloader').hide();
			});
		});
	}
}

$(document).ready(function() {
	new ApiLogin();
});